



<!DOCTYPE html>
<html lang="en" class="">
  <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# object: http://ogp.me/ns/object# article: http://ogp.me/ns/article# profile: http://ogp.me/ns/profile#">
    <meta charset='utf-8'>

    <link crossorigin="anonymous" href="https://assets-cdn.github.com/assets/frameworks-46094950e058d2cf30542982edfa17467b40fef171a28a786bdae50fab15526e.css" media="all" rel="stylesheet" />
    <link crossorigin="anonymous" href="https://assets-cdn.github.com/assets/github-65eae0e61ee036b2612ade2e43f38c8dea398a79f76be2aa2abebc4e52d4ccbe.css" media="all" rel="stylesheet" />
    
    
    <link crossorigin="anonymous" href="https://assets-cdn.github.com/assets/site-3b27741f2e22b48854ecee715112d86949a7c9e4e502f41739c4578cc65283da.css" media="all" rel="stylesheet" />
    
    

    <link as="script" href="https://assets-cdn.github.com/assets/frameworks-f2de4c863a487a877150989f965232bacfde178abf9c1963d9f84c5c19916f0c.js" rel="preload" />
    
    <link as="script" href="https://assets-cdn.github.com/assets/github-8922cd0a72e7d62c01c9622b2ac8c62afcd4114585de13349346cc789d65ad81.js" rel="preload" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta name="viewport" content="width=device-width">
    
    
    <title>ocaml.org/teaching-ocaml.md at master · ocaml/ocaml.org · GitHub</title>
    <link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml" title="GitHub">
    <link rel="fluid-icon" href="https://github.com/fluidicon.png" title="GitHub">
    <link rel="apple-touch-icon" href="/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
    <meta property="fb:app_id" content="1401488693436528">

      <meta content="https://avatars3.githubusercontent.com/u/1841483?v=3&amp;s=400" name="twitter:image:src" /><meta content="@github" name="twitter:site" /><meta content="summary" name="twitter:card" /><meta content="ocaml/ocaml.org" name="twitter:title" /><meta content="Implementation of the ocaml.org website." name="twitter:description" />
      <meta content="https://avatars3.githubusercontent.com/u/1841483?v=3&amp;s=400" property="og:image" /><meta content="GitHub" property="og:site_name" /><meta content="object" property="og:type" /><meta content="ocaml/ocaml.org" property="og:title" /><meta content="https://github.com/ocaml/ocaml.org" property="og:url" /><meta content="Implementation of the ocaml.org website." property="og:description" />
      <meta name="browser-stats-url" content="https://api.github.com/_private/browser/stats">
    <meta name="browser-errors-url" content="https://api.github.com/_private/browser/errors">
    <link rel="assets" href="https://assets-cdn.github.com/">
    
    <meta name="pjax-timeout" content="1000">
    

    <meta name="msapplication-TileImage" content="/windows-tile.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="selected-link" value="repo_source" data-pjax-transient>

    <meta name="google-site-verification" content="KT5gs8h0wvaagLKAVWq8bbeNwnZZK1r1XQysX3xurLU">
<meta name="google-site-verification" content="ZzhVyEFwb7w3e0-uOTltm8Jsck2F5StVihD0exw2fsA">
    <meta name="google-analytics" content="UA-3769691-2">

<meta content="collector.githubapp.com" name="octolytics-host" /><meta content="github" name="octolytics-app-id" /><meta content="6ABA759E:65AA:D2282B8:577CBEC2" name="octolytics-dimension-request_id" />
<meta content="/&lt;user-name&gt;/&lt;repo-name&gt;/blob/show" data-pjax-transient="true" name="analytics-location" />



  <meta class="js-ga-set" name="dimension1" content="Logged Out">



        <meta name="hostname" content="github.com">
    <meta name="user-login" content="">

        <meta name="expected-hostname" content="github.com">
      <meta name="js-proxy-site-detection-payload" content="M2VmODE0NTM5N2NiMWJjNzYwYWY2NTEyYWYyZWQ4Mjg5ODZiNDc2MDUxODMwM2U3M2YxMjVkOTlkYjUzOTgyNHx7InJlbW90ZV9hZGRyZXNzIjoiMTA2LjE4Ni4xMTcuMTU4IiwicmVxdWVzdF9pZCI6IjZBQkE3NTlFOjY1QUE6RDIyODJCODo1NzdDQkVDMiIsInRpbWVzdGFtcCI6MTQ2Nzc5MzA5Mn0=">


      <link rel="mask-icon" href="https://assets-cdn.github.com/pinned-octocat.svg" color="#4078c0">
      <link rel="icon" type="image/x-icon" href="https://assets-cdn.github.com/favicon.ico">

    <meta name="html-safe-nonce" content="6cd78ead968d0c4a92dc7b37715b6abb362447d3">
    <meta content="05eef49571c03c2634e3eb379d6fbeb853352a8b" name="form-nonce" />

    <meta http-equiv="x-pjax-version" content="d43371b6616f0e920351e907f7dc41c3">
    

      
  <meta name="description" content="Implementation of the ocaml.org website.">
  <meta name="go-import" content="github.com/ocaml/ocaml.org git https://github.com/ocaml/ocaml.org.git">

  <meta content="1841483" name="octolytics-dimension-user_id" /><meta content="ocaml" name="octolytics-dimension-user_login" /><meta content="3417777" name="octolytics-dimension-repository_id" /><meta content="ocaml/ocaml.org" name="octolytics-dimension-repository_nwo" /><meta content="true" name="octolytics-dimension-repository_public" /><meta content="false" name="octolytics-dimension-repository_is_fork" /><meta content="3417777" name="octolytics-dimension-repository_network_root_id" /><meta content="ocaml/ocaml.org" name="octolytics-dimension-repository_network_root_nwo" />
  <link href="https://github.com/ocaml/ocaml.org/commits/master.atom" rel="alternate" title="Recent Commits to ocaml.org:master" type="application/atom+xml">


      <link rel="canonical" href="https://github.com/ocaml/ocaml.org/blob/master/site/learn/teaching-ocaml.md" data-pjax-transient>
  </head>


  <body class="logged-out   env-production  vis-public page-blob">
    <div id="js-pjax-loader-bar" class="pjax-loader-bar"></div>
    <a href="#start-of-content" tabindex="1" class="accessibility-aid js-skip-to-content">Skip to content</a>

    
    
    



          <header class="site-header js-details-container" role="banner">
  <div class="container-responsive">
    <a class="header-logo-invertocat" href="https://github.com/" aria-label="Homepage" data-ga-click="(Logged out) Header, go to homepage, icon:logo-wordmark">
      <svg aria-hidden="true" class="octicon octicon-mark-github" height="32" version="1.1" viewBox="0 0 16 16" width="32"><path d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0 0 16 8c0-4.42-3.58-8-8-8z"></path></svg>
    </a>

    <button class="btn-link right site-header-toggle js-details-target" type="button" aria-label="Toggle navigation">
      <svg aria-hidden="true" class="octicon octicon-three-bars" height="24" version="1.1" viewBox="0 0 12 16" width="18"><path d="M11.41 9H.59C0 9 0 8.59 0 8c0-.59 0-1 .59-1H11.4c.59 0 .59.41.59 1 0 .59 0 1-.59 1h.01zm0-4H.59C0 5 0 4.59 0 4c0-.59 0-1 .59-1H11.4c.59 0 .59.41.59 1 0 .59 0 1-.59 1h.01zM.59 11H11.4c.59 0 .59.41.59 1 0 .59 0 1-.59 1H.59C0 13 0 12.59 0 12c0-.59 0-1 .59-1z"></path></svg>
    </button>

    <div class="site-header-menu">
      <nav class="site-header-nav site-header-nav-main">
        <a href="/personal" class="js-selected-navigation-item nav-item nav-item-personal" data-ga-click="Header, click, Nav menu - item:personal" data-selected-links="/personal /personal">
          Personal
</a>        <a href="/open-source" class="js-selected-navigation-item nav-item nav-item-opensource" data-ga-click="Header, click, Nav menu - item:opensource" data-selected-links="/open-source /open-source">
          Open source
</a>        <a href="/business" class="js-selected-navigation-item nav-item nav-item-business" data-ga-click="Header, click, Nav menu - item:business" data-selected-links="/business /business/features /business/customers /business">
          Business
</a>        <a href="/explore" class="js-selected-navigation-item nav-item nav-item-explore" data-ga-click="Header, click, Nav menu - item:explore" data-selected-links="/explore /trending /trending/developers /integrations /integrations/feature/code /integrations/feature/collaborate /integrations/feature/ship /explore">
          Explore
</a>      </nav>

      <div class="site-header-actions">
            <a class="btn btn-primary site-header-actions-btn" href="/join?source=header-repo" data-ga-click="(Logged out) Header, clicked Sign up, text:sign-up">Sign up</a>
          <a class="btn site-header-actions-btn mr-2" href="/login?return_to=%2Focaml%2Focaml.org%2Fblob%2Fmaster%2Fsite%2Flearn%2Fteaching-ocaml.md" data-ga-click="(Logged out) Header, clicked Sign in, text:sign-in">Sign in</a>
      </div>

        <nav class="site-header-nav site-header-nav-secondary">
          <a class="nav-item" href="/pricing">Pricing</a>
          <a class="nav-item" href="/blog">Blog</a>
          <a class="nav-item" href="https://help.github.com">Support</a>
          <a class="nav-item header-search-link" href="https://github.com/search">Search GitHub</a>
              <div class="header-search scoped-search site-scoped-search js-site-search" role="search">
  <!-- </textarea> --><!-- '"` --><form accept-charset="UTF-8" action="/ocaml/ocaml.org/search" class="js-site-search-form" data-scoped-search-url="/ocaml/ocaml.org/search" data-unscoped-search-url="/search" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
    <label class="form-control header-search-wrapper js-chromeless-input-container">
      <div class="header-search-scope">This repository</div>
      <input type="text"
        class="form-control header-search-input js-site-search-focus js-site-search-field is-clearable"
        data-hotkey="s"
        name="q"
        placeholder="Search"
        aria-label="Search this repository"
        data-unscoped-placeholder="Search GitHub"
        data-scoped-placeholder="Search"
        tabindex="1"
        autocapitalize="off">
    </label>
</form></div>

        </nav>
    </div>
  </div>
</header>



    <div id="start-of-content" class="accessibility-aid"></div>

      <div id="js-flash-container">
</div>


    <div role="main" class="main-content">
        <div itemscope itemtype="http://schema.org/SoftwareSourceCode">
    <div id="js-repo-pjax-container" data-pjax-container>
      
<div class="pagehead repohead instapaper_ignore readability-menu experiment-repo-nav">
  <div class="container repohead-details-container">

    

<ul class="pagehead-actions">

  <li>
      <a href="/login?return_to=%2Focaml%2Focaml.org"
    class="btn btn-sm btn-with-count tooltipped tooltipped-n"
    aria-label="You must be signed in to watch a repository" rel="nofollow">
    <svg aria-hidden="true" class="octicon octicon-eye" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M8.06 2C3 2 0 8 0 8s3 6 8.06 6C13 14 16 8 16 8s-3-6-7.94-6zM8 12c-2.2 0-4-1.78-4-4 0-2.2 1.8-4 4-4 2.22 0 4 1.8 4 4 0 2.22-1.78 4-4 4zm2-4c0 1.11-.89 2-2 2-1.11 0-2-.89-2-2 0-1.11.89-2 2-2 1.11 0 2 .89 2 2z"></path></svg>
    Watch
  </a>
  <a class="social-count" href="/ocaml/ocaml.org/watchers">
    29
  </a>

  </li>

  <li>
      <a href="/login?return_to=%2Focaml%2Focaml.org"
    class="btn btn-sm btn-with-count tooltipped tooltipped-n"
    aria-label="You must be signed in to star a repository" rel="nofollow">
    <svg aria-hidden="true" class="octicon octicon-star" height="16" version="1.1" viewBox="0 0 14 16" width="14"><path d="M14 6l-4.9-.64L7 1 4.9 5.36 0 6l3.6 3.26L2.67 14 7 11.67 11.33 14l-.93-4.74z"></path></svg>
    Star
  </a>

    <a class="social-count js-social-count" href="/ocaml/ocaml.org/stargazers">
      131
    </a>

  </li>

  <li>
      <a href="/login?return_to=%2Focaml%2Focaml.org"
        class="btn btn-sm btn-with-count tooltipped tooltipped-n"
        aria-label="You must be signed in to fork a repository" rel="nofollow">
        <svg aria-hidden="true" class="octicon octicon-repo-forked" height="16" version="1.1" viewBox="0 0 10 16" width="10"><path d="M8 1a1.993 1.993 0 0 0-1 3.72V6L5 8 3 6V4.72A1.993 1.993 0 0 0 2 1a1.993 1.993 0 0 0-1 3.72V6.5l3 3v1.78A1.993 1.993 0 0 0 5 15a1.993 1.993 0 0 0 1-3.72V9.5l3-3V4.72A1.993 1.993 0 0 0 8 1zM2 4.2C1.34 4.2.8 3.65.8 3c0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2zm3 10c-.66 0-1.2-.55-1.2-1.2 0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2zm3-10c-.66 0-1.2-.55-1.2-1.2 0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2z"></path></svg>
        Fork
      </a>

    <a href="/ocaml/ocaml.org/network" class="social-count">
      173
    </a>
  </li>
</ul>

    <h1 class="public ">
  <svg aria-hidden="true" class="octicon octicon-repo" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M4 9H3V8h1v1zm0-3H3v1h1V6zm0-2H3v1h1V4zm0-2H3v1h1V2zm8-1v12c0 .55-.45 1-1 1H6v2l-1.5-1.5L3 16v-2H1c-.55 0-1-.45-1-1V1c0-.55.45-1 1-1h10c.55 0 1 .45 1 1zm-1 10H1v2h2v-1h3v1h5v-2zm0-10H2v9h9V1z"></path></svg>
  <span class="author" itemprop="author"><a href="/ocaml" class="url fn" rel="author">ocaml</a></span><!--
--><span class="path-divider">/</span><!--
--><strong itemprop="name"><a href="/ocaml/ocaml.org" data-pjax="#js-repo-pjax-container">ocaml.org</a></strong>

</h1>

  </div>
  <div class="container">
    
<nav class="reponav js-repo-nav js-sidenav-container-pjax"
     itemscope
     itemtype="http://schema.org/BreadcrumbList"
     role="navigation"
     data-pjax="#js-repo-pjax-container">

  <span itemscope itemtype="http://schema.org/ListItem" itemprop="itemListElement">
    <a href="/ocaml/ocaml.org" aria-selected="true" class="js-selected-navigation-item selected reponav-item" data-hotkey="g c" data-selected-links="repo_source repo_downloads repo_commits repo_releases repo_tags repo_branches /ocaml/ocaml.org" itemprop="url">
      <svg aria-hidden="true" class="octicon octicon-code" height="16" version="1.1" viewBox="0 0 14 16" width="14"><path d="M9.5 3L8 4.5 11.5 8 8 11.5 9.5 13 14 8 9.5 3zm-5 0L0 8l4.5 5L6 11.5 2.5 8 6 4.5 4.5 3z"></path></svg>
      <span itemprop="name">Code</span>
      <meta itemprop="position" content="1">
</a>  </span>

    <span itemscope itemtype="http://schema.org/ListItem" itemprop="itemListElement">
      <a href="/ocaml/ocaml.org/issues" class="js-selected-navigation-item reponav-item" data-hotkey="g i" data-selected-links="repo_issues repo_labels repo_milestones /ocaml/ocaml.org/issues" itemprop="url">
        <svg aria-hidden="true" class="octicon octicon-issue-opened" height="16" version="1.1" viewBox="0 0 14 16" width="14"><path d="M7 2.3c3.14 0 5.7 2.56 5.7 5.7s-2.56 5.7-5.7 5.7A5.71 5.71 0 0 1 1.3 8c0-3.14 2.56-5.7 5.7-5.7zM7 1C3.14 1 0 4.14 0 8s3.14 7 7 7 7-3.14 7-7-3.14-7-7-7zm1 3H6v5h2V4zm0 6H6v2h2v-2z"></path></svg>
        <span itemprop="name">Issues</span>
        <span class="counter">114</span>
        <meta itemprop="position" content="2">
</a>    </span>

  <span itemscope itemtype="http://schema.org/ListItem" itemprop="itemListElement">
    <a href="/ocaml/ocaml.org/pulls" class="js-selected-navigation-item reponav-item" data-hotkey="g p" data-selected-links="repo_pulls /ocaml/ocaml.org/pulls" itemprop="url">
      <svg aria-hidden="true" class="octicon octicon-git-pull-request" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M11 11.28V5c-.03-.78-.34-1.47-.94-2.06C9.46 2.35 8.78 2.03 8 2H7V0L4 3l3 3V4h1c.27.02.48.11.69.31.21.2.3.42.31.69v6.28A1.993 1.993 0 0 0 10 15a1.993 1.993 0 0 0 1-3.72zm-1 2.92c-.66 0-1.2-.55-1.2-1.2 0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2zM4 3c0-1.11-.89-2-2-2a1.993 1.993 0 0 0-1 3.72v6.56A1.993 1.993 0 0 0 2 15a1.993 1.993 0 0 0 1-3.72V4.72c.59-.34 1-.98 1-1.72zm-.8 10c0 .66-.55 1.2-1.2 1.2-.65 0-1.2-.55-1.2-1.2 0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2zM2 4.2C1.34 4.2.8 3.65.8 3c0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2z"></path></svg>
      <span itemprop="name">Pull requests</span>
      <span class="counter">3</span>
      <meta itemprop="position" content="3">
</a>  </span>

    <a href="/ocaml/ocaml.org/wiki" class="js-selected-navigation-item reponav-item" data-hotkey="g w" data-selected-links="repo_wiki /ocaml/ocaml.org/wiki">
      <svg aria-hidden="true" class="octicon octicon-book" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M3 5h4v1H3V5zm0 3h4V7H3v1zm0 2h4V9H3v1zm11-5h-4v1h4V5zm0 2h-4v1h4V7zm0 2h-4v1h4V9zm2-6v9c0 .55-.45 1-1 1H9.5l-1 1-1-1H2c-.55 0-1-.45-1-1V3c0-.55.45-1 1-1h5.5l1 1 1-1H15c.55 0 1 .45 1 1zm-8 .5L7.5 3H2v9h6V3.5zm7-.5H9.5l-.5.5V12h6V3z"></path></svg>
      Wiki
</a>

  <a href="/ocaml/ocaml.org/pulse" class="js-selected-navigation-item reponav-item" data-selected-links="pulse /ocaml/ocaml.org/pulse">
    <svg aria-hidden="true" class="octicon octicon-pulse" height="16" version="1.1" viewBox="0 0 14 16" width="14"><path d="M11.5 8L8.8 5.4 6.6 8.5 5.5 1.6 2.38 8H0v2h3.6l.9-1.8.9 5.4L9 8.5l1.6 1.5H14V8z"></path></svg>
    Pulse
</a>
  <a href="/ocaml/ocaml.org/graphs" class="js-selected-navigation-item reponav-item" data-selected-links="repo_graphs repo_contributors /ocaml/ocaml.org/graphs">
    <svg aria-hidden="true" class="octicon octicon-graph" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M16 14v1H0V0h1v14h15zM5 13H3V8h2v5zm4 0H7V3h2v10zm4 0h-2V6h2v7z"></path></svg>
    Graphs
</a>

</nav>

  </div>
</div>

<div class="container new-discussion-timeline experiment-repo-nav">
  <div class="repository-content">

    

<a href="/ocaml/ocaml.org/blob/6e9323604d841d4ce010893b11d69830b9934f75/site/learn/teaching-ocaml.md" class="hidden js-permalink-shortcut" data-hotkey="y">Permalink</a>

<!-- blob contrib key: blob_contributors:v21:43578d3a0267992f122e8688e6cc874c -->

<div class="file-navigation js-zeroclipboard-container">
  
<div class="select-menu branch-select-menu js-menu-container js-select-menu left">
  <button class="btn btn-sm select-menu-button js-menu-target css-truncate" data-hotkey="w"
    title="master"
    type="button" aria-label="Switch branches or tags" tabindex="0" aria-haspopup="true">
    <i>Branch:</i>
    <span class="js-select-button css-truncate-target">master</span>
  </button>

  <div class="select-menu-modal-holder js-menu-content js-navigation-container" data-pjax aria-hidden="true">

    <div class="select-menu-modal">
      <div class="select-menu-header">
        <svg aria-label="Close" class="octicon octicon-x js-menu-close" height="16" role="img" version="1.1" viewBox="0 0 12 16" width="12"><path d="M7.48 8l3.75 3.75-1.48 1.48L6 9.48l-3.75 3.75-1.48-1.48L4.52 8 .77 4.25l1.48-1.48L6 6.52l3.75-3.75 1.48 1.48z"></path></svg>
        <span class="select-menu-title">Switch branches/tags</span>
      </div>

      <div class="select-menu-filters">
        <div class="select-menu-text-filter">
          <input type="text" aria-label="Filter branches/tags" id="context-commitish-filter-field" class="form-control js-filterable-field js-navigation-enable" placeholder="Filter branches/tags">
        </div>
        <div class="select-menu-tabs">
          <ul>
            <li class="select-menu-tab">
              <a href="#" data-tab-filter="branches" data-filter-placeholder="Filter branches/tags" class="js-select-menu-tab" role="tab">Branches</a>
            </li>
            <li class="select-menu-tab">
              <a href="#" data-tab-filter="tags" data-filter-placeholder="Find a tag…" class="js-select-menu-tab" role="tab">Tags</a>
            </li>
          </ul>
        </div>
      </div>

      <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket" data-tab-filter="branches" role="menu">

        <div data-filterable-for="context-commitish-filter-field" data-filterable-type="substring">


            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/ocaml/ocaml.org/blob/cohttp/site/learn/teaching-ocaml.md"
               data-name="cohttp"
               data-skip-pjax="true"
               rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5l-8 8-4-4 1.5-1.5L4 10l6.5-6.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target js-select-menu-filter-text" title="cohttp">
                cohttp
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open selected"
               href="/ocaml/ocaml.org/blob/master/site/learn/teaching-ocaml.md"
               data-name="master"
               data-skip-pjax="true"
               rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5l-8 8-4-4 1.5-1.5L4 10l6.5-6.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target js-select-menu-filter-text" title="master">
                master
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/ocaml/ocaml.org/blob/oloop/site/learn/teaching-ocaml.md"
               data-name="oloop"
               data-skip-pjax="true"
               rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5l-8 8-4-4 1.5-1.5L4 10l6.5-6.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target js-select-menu-filter-text" title="oloop">
                oloop
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/ocaml/ocaml.org/blob/omd/site/learn/teaching-ocaml.md"
               data-name="omd"
               data-skip-pjax="true"
               rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5l-8 8-4-4 1.5-1.5L4 10l6.5-6.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target js-select-menu-filter-text" title="omd">
                omd
              </span>
            </a>
        </div>

          <div class="select-menu-no-results">Nothing to show</div>
      </div>

      <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket" data-tab-filter="tags">
        <div data-filterable-for="context-commitish-filter-field" data-filterable-type="substring">


            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/ocaml/ocaml.org/tree/1.0/site/learn/teaching-ocaml.md"
              data-name="1.0"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5l-8 8-4-4 1.5-1.5L4 10l6.5-6.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="1.0">
                1.0
              </span>
            </a>
        </div>

        <div class="select-menu-no-results">Nothing to show</div>
      </div>

    </div>
  </div>
</div>

  <div class="btn-group right">
    <a href="/ocaml/ocaml.org/find/master"
          class="js-pjax-capture-input btn btn-sm"
          data-pjax
          data-hotkey="t">
      Find file
    </a>
    <button aria-label="Copy file path to clipboard" class="js-zeroclipboard btn btn-sm zeroclipboard-button tooltipped tooltipped-s" data-copied-hint="Copied!" type="button">Copy path</button>
  </div>
  <div class="breadcrumb js-zeroclipboard-target">
    <span class="repo-root js-repo-root"><span class="js-path-segment"><a href="/ocaml/ocaml.org"><span>ocaml.org</span></a></span></span><span class="separator">/</span><span class="js-path-segment"><a href="/ocaml/ocaml.org/tree/master/site"><span>site</span></a></span><span class="separator">/</span><span class="js-path-segment"><a href="/ocaml/ocaml.org/tree/master/site/learn"><span>learn</span></a></span><span class="separator">/</span><strong class="final-path">teaching-ocaml.md</strong>
  </div>
</div>


  <div class="commit-tease">
      <span class="right">
        <a class="commit-tease-sha" href="/ocaml/ocaml.org/commit/b456895e4084fad194a05d1e3b8b1b5fc05afb36" data-pjax>
          b456895
        </a>
        <relative-time datetime="2016-03-25T16:09:36Z">Mar 25, 2016</relative-time>
      </span>
      <div>
        <img alt="@yminsky" class="avatar" height="20" src="https://avatars2.githubusercontent.com/u/715302?v=3&amp;s=40" width="20" />
        <a href="/yminsky" class="user-mention" rel="contributor">yminsky</a>
          <a href="/ocaml/ocaml.org/commit/b456895e4084fad194a05d1e3b8b1b5fc05afb36" class="message" data-pjax="true" title="reorganize teaching page with regions">reorganize teaching page with regions</a>
      </div>

    <div class="commit-tease-contributors">
      <button type="button" class="btn-link muted-link contributors-toggle" data-facebox="#blob_contributors_box">
        <strong>16</strong>
         contributors
      </button>
          <a class="avatar-link tooltipped tooltipped-s" aria-label="pw374" href="/ocaml/ocaml.org/commits/master/site/learn/teaching-ocaml.md?author=pw374"><img alt="@pw374" class="avatar" height="20" src="https://avatars3.githubusercontent.com/u/3776012?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="amirmc" href="/ocaml/ocaml.org/commits/master/site/learn/teaching-ocaml.md?author=amirmc"><img alt="@amirmc" class="avatar" height="20" src="https://avatars0.githubusercontent.com/u/121855?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="dsheets" href="/ocaml/ocaml.org/commits/master/site/learn/teaching-ocaml.md?author=dsheets"><img alt="@dsheets" class="avatar" height="20" src="https://avatars2.githubusercontent.com/u/59918?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="yminsky" href="/ocaml/ocaml.org/commits/master/site/learn/teaching-ocaml.md?author=yminsky"><img alt="@yminsky" class="avatar" height="20" src="https://avatars2.githubusercontent.com/u/715302?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="abcde13" href="/ocaml/ocaml.org/commits/master/site/learn/teaching-ocaml.md?author=abcde13"><img alt="@abcde13" class="avatar" height="20" src="https://avatars2.githubusercontent.com/u/894122?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="nealwu" href="/ocaml/ocaml.org/commits/master/site/learn/teaching-ocaml.md?author=nealwu"><img alt="@nealwu" class="avatar" height="20" src="https://avatars1.githubusercontent.com/u/726075?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="mrecachinas" href="/ocaml/ocaml.org/commits/master/site/learn/teaching-ocaml.md?author=mrecachinas"><img alt="@mrecachinas" class="avatar" height="20" src="https://avatars2.githubusercontent.com/u/924243?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="octref" href="/ocaml/ocaml.org/commits/master/site/learn/teaching-ocaml.md?author=octref"><img alt="@octref" class="avatar" height="20" src="https://avatars2.githubusercontent.com/u/4033249?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="jhn" href="/ocaml/ocaml.org/commits/master/site/learn/teaching-ocaml.md?author=jhn"><img alt="@jhn" class="avatar" height="20" src="https://avatars1.githubusercontent.com/u/678798?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="Guyslain" href="/ocaml/ocaml.org/commits/master/site/learn/teaching-ocaml.md?author=Guyslain"><img alt="@Guyslain" class="avatar" height="20" src="https://avatars3.githubusercontent.com/u/1467216?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="rdicosmo" href="/ocaml/ocaml.org/commits/master/site/learn/teaching-ocaml.md?author=rdicosmo"><img alt="@rdicosmo" class="avatar" height="20" src="https://avatars3.githubusercontent.com/u/687807?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="Chris00" href="/ocaml/ocaml.org/commits/master/site/learn/teaching-ocaml.md?author=Chris00"><img alt="@Chris00" class="avatar" height="20" src="https://avatars2.githubusercontent.com/u/1255665?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="chenglou" href="/ocaml/ocaml.org/commits/master/site/learn/teaching-ocaml.md?author=chenglou"><img alt="@chenglou" class="avatar" height="20" src="https://avatars2.githubusercontent.com/u/1909539?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="grayswandyr" href="/ocaml/ocaml.org/commits/master/site/learn/teaching-ocaml.md?author=grayswandyr"><img alt="@grayswandyr" class="avatar" height="20" src="https://avatars2.githubusercontent.com/u/8328276?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="yansh" href="/ocaml/ocaml.org/commits/master/site/learn/teaching-ocaml.md?author=yansh"><img alt="@yansh" class="avatar" height="20" src="https://avatars0.githubusercontent.com/u/1371050?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="atestu" href="/ocaml/ocaml.org/commits/master/site/learn/teaching-ocaml.md?author=atestu"><img alt="@atestu" class="avatar" height="20" src="https://avatars2.githubusercontent.com/u/22851?v=3&amp;s=40" width="20" /> </a>


    </div>

    <div id="blob_contributors_box" style="display:none">
      <h2 class="facebox-header" data-facebox-id="facebox-header">Users who have contributed to this file</h2>
      <ul class="facebox-user-list" data-facebox-id="facebox-description">
          <li class="facebox-user-list-item">
            <img alt="@pw374" height="24" src="https://avatars1.githubusercontent.com/u/3776012?v=3&amp;s=48" width="24" />
            <a href="/pw374">pw374</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@amirmc" height="24" src="https://avatars2.githubusercontent.com/u/121855?v=3&amp;s=48" width="24" />
            <a href="/amirmc">amirmc</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@dsheets" height="24" src="https://avatars0.githubusercontent.com/u/59918?v=3&amp;s=48" width="24" />
            <a href="/dsheets">dsheets</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@yminsky" height="24" src="https://avatars0.githubusercontent.com/u/715302?v=3&amp;s=48" width="24" />
            <a href="/yminsky">yminsky</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@abcde13" height="24" src="https://avatars0.githubusercontent.com/u/894122?v=3&amp;s=48" width="24" />
            <a href="/abcde13">abcde13</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@nealwu" height="24" src="https://avatars3.githubusercontent.com/u/726075?v=3&amp;s=48" width="24" />
            <a href="/nealwu">nealwu</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@mrecachinas" height="24" src="https://avatars0.githubusercontent.com/u/924243?v=3&amp;s=48" width="24" />
            <a href="/mrecachinas">mrecachinas</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@octref" height="24" src="https://avatars0.githubusercontent.com/u/4033249?v=3&amp;s=48" width="24" />
            <a href="/octref">octref</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@jhn" height="24" src="https://avatars3.githubusercontent.com/u/678798?v=3&amp;s=48" width="24" />
            <a href="/jhn">jhn</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@Guyslain" height="24" src="https://avatars1.githubusercontent.com/u/1467216?v=3&amp;s=48" width="24" />
            <a href="/Guyslain">Guyslain</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@rdicosmo" height="24" src="https://avatars1.githubusercontent.com/u/687807?v=3&amp;s=48" width="24" />
            <a href="/rdicosmo">rdicosmo</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@Chris00" height="24" src="https://avatars0.githubusercontent.com/u/1255665?v=3&amp;s=48" width="24" />
            <a href="/Chris00">Chris00</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@chenglou" height="24" src="https://avatars0.githubusercontent.com/u/1909539?v=3&amp;s=48" width="24" />
            <a href="/chenglou">chenglou</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@grayswandyr" height="24" src="https://avatars0.githubusercontent.com/u/8328276?v=3&amp;s=48" width="24" />
            <a href="/grayswandyr">grayswandyr</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@yansh" height="24" src="https://avatars2.githubusercontent.com/u/1371050?v=3&amp;s=48" width="24" />
            <a href="/yansh">yansh</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@atestu" height="24" src="https://avatars0.githubusercontent.com/u/22851?v=3&amp;s=48" width="24" />
            <a href="/atestu">atestu</a>
          </li>
      </ul>
    </div>
  </div>

<div class="file">
  <div class="file-header">
  <div class="file-actions">

    <div class="btn-group">
      <a href="/ocaml/ocaml.org/raw/master/site/learn/teaching-ocaml.md" class="btn btn-sm " id="raw-url">Raw</a>
        <a href="/ocaml/ocaml.org/blame/master/site/learn/teaching-ocaml.md" class="btn btn-sm js-update-url-with-hash">Blame</a>
      <a href="/ocaml/ocaml.org/commits/master/site/learn/teaching-ocaml.md" class="btn btn-sm " rel="nofollow">History</a>
    </div>


        <button type="button" class="btn-octicon disabled tooltipped tooltipped-nw"
          aria-label="You must be signed in to make or propose changes">
          <svg aria-hidden="true" class="octicon octicon-pencil" height="16" version="1.1" viewBox="0 0 14 16" width="14"><path d="M0 12v3h3l8-8-3-3-8 8zm3 2H1v-2h1v1h1v1zm10.3-9.3L12 6 9 3l1.3-1.3a.996.996 0 0 1 1.41 0l1.59 1.59c.39.39.39 1.02 0 1.41z"></path></svg>
        </button>
        <button type="button" class="btn-octicon btn-octicon-danger disabled tooltipped tooltipped-nw"
          aria-label="You must be signed in to make or propose changes">
          <svg aria-hidden="true" class="octicon octicon-trashcan" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M11 2H9c0-.55-.45-1-1-1H5c-.55 0-1 .45-1 1H2c-.55 0-1 .45-1 1v1c0 .55.45 1 1 1v9c0 .55.45 1 1 1h7c.55 0 1-.45 1-1V5c.55 0 1-.45 1-1V3c0-.55-.45-1-1-1zm-1 12H3V5h1v8h1V5h1v8h1V5h1v8h1V5h1v9zm1-10H2V3h9v1z"></path></svg>
        </button>
  </div>

  <div class="file-info">
      165 lines (116 sloc)
      <span class="file-info-divider"></span>
    10.7 KB
  </div>
</div>

  
  <div id="readme" class="readme blob instapaper_body">
    <article class="markdown-body entry-content" itemprop="text">

<h1><a id="user-content-teaching-ocaml" class="anchor" href="#teaching-ocaml" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Teaching OCaml</h1>

<p>OCaml is a high-level language that supports functional, imperative
and object-oriented programming styles. As such, it is an excellent
tool for teaching the fundamental concepts behind programming
languages, type theory and systems.</p>

<p>For the full description of the benefits of the OCaml language
please refer to the article on
“<a href="https://realworldocaml.org/v1/en/html/prologue.html">Why OCaml?</a>”.</p>

<p>This page is devoted to resources for people who are teaching OCaml in
a University setting. It contains a list of courses in different
Universities that use OCaml as a teaching language. It can help with
setting up a course structure as well as finding the relevant contacts
to discuss and share experiences. We also include relevant resources
such as references to commonly used textbooks and tools for
setting up your preferred environment to work with OCaml. We hope that
this will make the task of shaping a programming course that uses
OCaml a slightly easier task.</p>

<p><strong>Don't worry if things do not work</strong>! OCaml has a vibrant and rapidly
  growing community behind it that is eager to help. If you find any
  problems with any of the tutorials or something is missing please
  let us know by
  <a href="http://lists.ocaml.org/listinfo/teaching">getting in touch</a> so we
  can help in making your experience with OCaml more pleasant. If you
  would like to add your course or other resources please feel free to
  <a href="https://github.com/ocaml/ocaml.org/issues/new">create an issue</a>.</p>

<h3><a id="user-content-mailing-list" class="anchor" href="#mailing-list" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Mailing list</h3>

<p>We set-up a <a href="http://lists.ocaml.org/listinfo/teaching">mailing list</a> for professors who are teaching OCaml in schools and universities to discuss issues relating to using OCaml in an educational context.</p>

<h2><a id="user-content-courses-taught-in-ocaml" class="anchor" href="#courses-taught-in-ocaml" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Courses taught in OCaml</h2>

<p>Here's a list of courses we know about that teach in OCaml.
Please add yours if you don't see it listed!</p>

<h3><a id="user-content-north-america" class="anchor" href="#north-america" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>North America</h3>

<ol>
<li><a href="http://www.cs.bc.edu/%7Emuller/teaching/cs110105/f14/">Boston College</a> - Computer Science I (CS 1101)</li>
<li><a href="http://cs.brown.edu/courses/cs017/">Brown University</a> - An Integrated introducion (CS 17/18)  (along with Racket, Scala and Java)</li>
<li><a href="http://users.cms.caltech.edu/%7Emvanier/cs4/Winter2016/">Caltech</a> - Fundamentals of Computer Programming</li>
<li><a href="http://www1.cs.columbia.edu/%7Esedwards/classes/2014/w4115-fall/index.html">Columbia University</a> - Programming Languages and Translators</li>
<li><a href="http://www.cs.cornell.edu/Courses/cs3110/2014fa/course_info.php">Cornell University</a> - Data Structures and Functional Programming (CS 3110)</li>
<li><a href="http://people.fas.harvard.edu/%7Elib153/">Harvard University</a> - Principles of Programming Language Compilation (CS153)</li>
<li><a href="http://www.fas.harvard.edu/%7Ecs51">Harvard University</a> - Introduction to Computer Science II: Abstraction &amp; Design (CS51)</li>
<li><a href="http://www.cs.mcgill.ca/%7Ebpientka/cs302/">McGill University</a> - Programming Languages and Paradigms (COMP 302)</li>
<li><a href="http://www.cs.princeton.edu/courses/archive/fall14/cos326/">Princeton University</a> - Functional Programming (COS 326)</li>
<li><a href="http://www.cs.rice.edu/%7Ejavaplt/311/info.html">Rice University</a> - Principles of Programming Languages (COMP 311)</li>
<li><a href="http://www.registrar.ucla.edu/schedule/subdet.aspx?term=14F&amp;srs=187510200">University of California, Los Angeles</a> - Programming Languages (along with Python and Java) (CS 131)</li>
<li><a href="http://cseweb.ucsd.edu/classes/wi14/cse130-a/">University of California, San Diego</a> - Programming Languages: Principles and Paradigms (CSE130-a)  (along with Python and Prolog)</li>
<li><a href="https://courses.engr.illinois.edu/cs421/fa2014/">University of Illinois at Urbana-Champaign</a> - Programming Languages and Compilers (CS 421)</li>
<li><a href="http://www.cs.umd.edu/class/spring2014/cmsc330/">University of Maryland</a> (along with Ruby, Prolog, Java) - Organization of Programming Languages (CMSC 330)</li>
<li><a href="http://people.cs.umass.edu/%7Earjun/teaching/631/">University of Massachusetts Amherst</a> - Programming Languages (CMPSCI 631)</li>
<li><a href="http://people.cs.umass.edu/%7Earjun/courses/cs691f/">University of Massachusetts</a> - Programming Languages (CS691F)</li>
<li><a href="http://www.cis.upenn.edu/%7Ecis341/current/">University of Pennsylvania</a> - Compilers  (CIS341)</li>
<li><a href="http://www.seas.upenn.edu/%7Ecis120/current/">University of Pennsylvania</a> - Programming Languages and Techniques I (CIS120)</li>
<li><a href="http://www.cs.virginia.edu/%7Eweimer/4610/">University of Virginia</a> - Programming Languages (CS 4610)</li>
</ol>

<h3><a id="user-content-europe" class="anchor" href="#europe" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Europe</h3>

<ol>
<li><a href="https://services.brics.dk/java/courseadmin/dOvs">Aarhus University</a> - The compilation course (along with Java)</li>
<li><a href="http://assert-false.net/callcc/Guyslain/Teaching/ProgFonc/index">Aix-Marseille University</a> - Functional Programming</li>
<li><a href="http://epita.fr/">Epita</a> - Introduction to Algorithms (Year 1 &amp; 2)</li>
<li><a href="http://supaero.isae.fr/en">ISAE/Supaéro</a> - Functional programming and introduction to type systems</li>
<li><a href="http://www-apr.lip6.fr/%7Echaillou/Public/enseignement/2014-2015/tas/">University Pierre &amp; Marie Curie</a> - Types and static analysis (5I555)</li>
<li><a href="http://www-licence.ufr-info-p6.jussieu.fr/lmd/licence/2014/ue/LI332-2014oct/">University Pierre &amp; Marie Curie</a> - Models of programming and languages interoperability (LI332)</li>
<li><a href="https://sites.google.com/site/focs1112/">University of Birmingham</a> - Foundations of Computer Science (FOCS1112)</li>
<li><a href="http://www.cl.cam.ac.uk/teaching/1415/L28/">University of Cambridge</a> - Advanced Functional Programming (L28)</li>
<li><a href="http://cl-informatik.uibk.ac.at/teaching/ss06/ocaml/schedule.php">University of Innsbruck </a> -  Programming in OCAML (SS 06)</li>
<li><a href="http://etudes.univ-rennes1.fr/masterInformatique/themes/PremiereAnnee/Programme/COMP">University of Rennes 1</a> - Compilation (COMP)</li>
<li><a href="http://etudes.univ-rennes1.fr/masterInformatique/themes/PremiereAnnee/Programme/SEM">University of Rennes 1</a> - Semantics (SEM)</li>
<li><a href="https://etudes.univ-rennes1.fr/licenceInformatique/themes/OrganisationEtudes/L3info/Programme">University of Rennes 1</a> - Programming 2 (PRG2)</li>
<li><a href="https://international.uni.wroc.pl/en/course/functional-programming">University of Wrocław</a> - Functional Programming</li>
<li><a href="http://www.dicosmo.org/CourseNotes/pfav/">Université Paris-Diderot</a> - Advanced Functional Programming (PFAV)</li>
<li><a href="http://www.pps.univ-paris-diderot.fr/%7Etreinen/teaching/pf5/">Université Paris-Diderot</a> - Functional Programming (PF5)</li>
</ol>

<h3><a id="user-content-asia" class="anchor" href="#asia" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Asia</h3>

<ol>
<li><a href="http://www.cse.iitd.ernet.in/%7Essen/csl101/details.html">Indian Institute of Technology, Delhi</a> - Introduction to Computers and Programming (CSL 101) (along with Pascal and Java)</li>
</ol>

<h3><a id="user-content-map-of-courses-around-the-world" class="anchor" href="#map-of-courses-around-the-world" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Map of courses around the world</h3>

<p><a href="https://www.google.com/maps/d/edit?mid=zk8_K4G_usic.kkzYvEvqV44Q">Add your course to the map!</a></p>

<p><em>(Tip: Click on the relevant layer (e.g., US,EU), search for your
University, click on the marker and click on "Add to map", fill in the
info fields and you are done!)</em></p>



<h1><a id="user-content-resources" class="anchor" href="#resources" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Resources</h1>

<h2><a id="user-content-suggested-textbooks" class="anchor" href="#suggested-textbooks" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Suggested Textbooks</h2>

<p>This is a list of books and lecture notes that are being used for
teaching OCaml. There is also a wider list of books to choose from, which you can find on the <a href="http://ocaml.org/learn/books.html">books page</a>.</p>

<ol>
<li><p><a href="http://caml.inria.fr/pub/docs/manual-ocaml/">The OCaml System: Documentation and User's Manual</a> This the official User's Manual. It serves as a complete reference guide to OCaml. Updated for each version of OCaml, it contains the description of the language, of its extensions, and the documentation of the tools and libraries included in the official distribution.</p></li>
<li><p><a href="http://www.seas.upenn.edu/%7Ecis120/current/notes/120notes.pdf">Lecture notes</a>
for Penn's CIS120.  Like the course, this book covers both OCaml
and Java, and is intended for students with no programming
background.</p></li>
<li><a href="http://shop.oreilly.com/product/0636920024743.do">Real World OCaml</a>,
also available <a href="https://realworldocaml.org/">online</a>.  Used by Harvard's CS51 course.</li>
<li>Jason Hickey's <a href="http://files.metaprl.org/doc/ocaml-book.pdf">Introduction to Objective Caml</a>,
originally developed as lecture notes for Jason's course at Caltech</li>
</ol>

<h2><a id="user-content-teaching-tools" class="anchor" href="#teaching-tools" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Teaching tools</h2>

<p>This section lists some of the available tools that are or can be used in a classroom or lab.</p>

<h3><a id="user-content-in-the-browser" class="anchor" href="#in-the-browser" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>In the browser</h3>

<p>The OCaml bytecode is relatively straightforward to convert to Javascript. This allows any OCaml code to run in a Web browser, which is very convenient when delivering individual tutorials to a class as there's minimal set up time involved.</p>

<p>For a quick example of OCaml in the browser, try any of the following:</p>

<ol>
<li><a href="https://github.com/andrewray/iocaml/blob/master/README.md">IOCaml Notebooks</a></li>
<li><a href="http://ocsigen.github.io/js_of_ocaml/">A compiler from OCaml bytecode to Javascript</a></li>
<li><a href="http://try.ocamlpro.com/">Try OCaml</a></li>
<li><a href="http://www.compileonline.com/compile_ocaml_online.php">Compile OCaml Online</a></li>
<li><a href="http://codepad.org/">Codepad</a></li>
<li><a href="https://dbgr.cc/l/ocaml">Run OCaml Code Online</a></li>
</ol>

<h3><a id="user-content-ocaml-installation" class="anchor" href="#ocaml-installation" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>OCaml installation</h3>

<p>Check out our <a href="http://ocaml.org/docs/install.html">Install OCaml section</a></p>

<p>Alternative guides</p>

<ol>
<li><a href="https://github.com/realworldocaml/book/wiki/Installation-Instructions">Installation Instructions from Real World OCaml</a></li>
</ol>

<h3><a id="user-content-vm-images" class="anchor" href="#vm-images" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>VM images</h3>

<p>This is a list of VM images that some Universities use to avoid setup
delays and provide a consistent environment.</p>

<ol>
<li><a href="http://caml.inria.fr/pub/distrib/ocaml-4.01/ocaml-4.01.0-intel.dmg">OCaml 4.01 for MAC</a></li>
<li><a href="https://cornell.app.box.com/s/acqwpvnidu5yq1osd8lb">Cornell's Linux VM VirtualBox image</a> (<a href="http://www.cs.cornell.edu/courses/CS3110/2014sp/hw/0/ps0.pdf">more info here</a>)</li>
<li><a href="http://www.cs.princeton.edu/%7Ecos326/Ubuntu326.ova">Princeton's Ubuntu VirtualBox VM</a> (more info on the course's <a href="http://www.cs.princeton.edu/courses/archive/fall14/cos326/resources.php">site</a>)</li>
<li><a href="https://github.com/mirage/mirage-vagrant-vms">Mirage Virtualbox VMs via Vagrant</a></li>
</ol>

<h3><a id="user-content-tutorials-and-exercises" class="anchor" href="#tutorials-and-exercises" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Tutorials and Exercises</h3>

<p><strong>University tutorials and notes</strong></p>

<ol>
<li><a href="http://people.cs.umass.edu/%7Earjun/courses/cs691f/assignments/ocaml-tutorial.html">From UMass's CS691F Programming languages course</a></li>
<li><a href="http://cseweb.ucsd.edu/classes/wi11/cse130/">CSE 130: Programming Languages Course at UCSA</a>. Click on <a href="http://cseweb.ucsd.edu/classes/wi11/cse130/">Lectures</a> and <a href="http://cseweb.ucsd.edu/classes/wi11/cse130/">Discussion Notes</a>. Have a look at Lec 7 (a pp presentation by Zach) for an inspiring introduction into map and fold.</li>
<li><a href="http://www.cs.cornell.edu/courses/cs3110/2013fa/lecture_notes.php">CS 3110 Fall 2013 :: Data Structures and Functional Programming at Cornell</a>. The lecturing materials explain OCaml on the basis of the fundamental concepts of functional programming languages.</li>
<li><a href="http://www.seas.upenn.edu/%7Ecis500/cis500-f05/index.html">CIS 500: Software Foundations at UPenn</a>. A Software Fundations course with strong emphasis on Types. The <a href="http://www.seas.upenn.edu/%7Ecis500/cis500-f05/resources/seas-ocaml.html">OCaml Tutorial</a> includes a gentle introduction to the OCaml interactive top-level system and compilation in OCaml.</li>
</ol>
</article>
  </div>

</div>

<button type="button" data-facebox="#jump-to-line" data-facebox-class="linejump" data-hotkey="l" class="hidden">Jump to Line</button>
<div id="jump-to-line" style="display:none">
  <!-- </textarea> --><!-- '"` --><form accept-charset="UTF-8" action="" class="js-jump-to-line-form" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
    <input class="form-control linejump-input js-jump-to-line-field" type="text" placeholder="Jump to line&hellip;" aria-label="Jump to line" autofocus>
    <button type="submit" class="btn">Go</button>
</form></div>

  </div>
  <div class="modal-backdrop js-touch-events"></div>
</div>


    </div>
  </div>

    </div>

        <div class="container site-footer-container">
  <div class="site-footer" role="contentinfo">
    <ul class="site-footer-links right">
        <li><a href="https://status.github.com/" data-ga-click="Footer, go to status, text:status">Status</a></li>
      <li><a href="https://developer.github.com" data-ga-click="Footer, go to api, text:api">API</a></li>
      <li><a href="https://training.github.com" data-ga-click="Footer, go to training, text:training">Training</a></li>
      <li><a href="https://shop.github.com" data-ga-click="Footer, go to shop, text:shop">Shop</a></li>
        <li><a href="https://github.com/blog" data-ga-click="Footer, go to blog, text:blog">Blog</a></li>
        <li><a href="https://github.com/about" data-ga-click="Footer, go to about, text:about">About</a></li>

    </ul>

    <a href="https://github.com" aria-label="Homepage" class="site-footer-mark" title="GitHub">
      <svg aria-hidden="true" class="octicon octicon-mark-github" height="24" version="1.1" viewBox="0 0 16 16" width="24"><path d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0 0 16 8c0-4.42-3.58-8-8-8z"></path></svg>
</a>
    <ul class="site-footer-links">
      <li>&copy; 2016 <span title="0.04599s from github-fe121-cp1-prd.iad.github.net">GitHub</span>, Inc.</li>
        <li><a href="https://github.com/site/terms" data-ga-click="Footer, go to terms, text:terms">Terms</a></li>
        <li><a href="https://github.com/site/privacy" data-ga-click="Footer, go to privacy, text:privacy">Privacy</a></li>
        <li><a href="https://github.com/security" data-ga-click="Footer, go to security, text:security">Security</a></li>
        <li><a href="https://github.com/contact" data-ga-click="Footer, go to contact, text:contact">Contact</a></li>
        <li><a href="https://help.github.com" data-ga-click="Footer, go to help, text:help">Help</a></li>
    </ul>
  </div>
</div>



    

    <div id="ajax-error-message" class="ajax-error-message flash flash-error">
      <svg aria-hidden="true" class="octicon octicon-alert" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M8.865 1.52c-.18-.31-.51-.5-.87-.5s-.69.19-.87.5L.275 13.5c-.18.31-.18.69 0 1 .19.31.52.5.87.5h13.7c.36 0 .69-.19.86-.5.17-.31.18-.69.01-1L8.865 1.52zM8.995 13h-2v-2h2v2zm0-3h-2V6h2v4z"></path></svg>
      <button type="button" class="flash-close js-flash-close js-ajax-error-dismiss" aria-label="Dismiss error">
        <svg aria-hidden="true" class="octicon octicon-x" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M7.48 8l3.75 3.75-1.48 1.48L6 9.48l-3.75 3.75-1.48-1.48L4.52 8 .77 4.25l1.48-1.48L6 6.52l3.75-3.75 1.48 1.48z"></path></svg>
      </button>
      Something went wrong with that request. Please try again.
    </div>


      <script crossorigin="anonymous" src="https://assets-cdn.github.com/assets/compat-7db58f8b7b91111107fac755dd8b178fe7db0f209ced51fc339c446ad3f8da2b.js"></script>
      <script crossorigin="anonymous" src="https://assets-cdn.github.com/assets/frameworks-f2de4c863a487a877150989f965232bacfde178abf9c1963d9f84c5c19916f0c.js"></script>
      <script async="async" crossorigin="anonymous" src="https://assets-cdn.github.com/assets/github-8922cd0a72e7d62c01c9622b2ac8c62afcd4114585de13349346cc789d65ad81.js"></script>
      
      
      
      
      
      
    <div class="js-stale-session-flash stale-session-flash flash flash-warn flash-banner hidden">
      <svg aria-hidden="true" class="octicon octicon-alert" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M8.865 1.52c-.18-.31-.51-.5-.87-.5s-.69.19-.87.5L.275 13.5c-.18.31-.18.69 0 1 .19.31.52.5.87.5h13.7c.36 0 .69-.19.86-.5.17-.31.18-.69.01-1L8.865 1.52zM8.995 13h-2v-2h2v2zm0-3h-2V6h2v4z"></path></svg>
      <span class="signed-in-tab-flash">You signed in with another tab or window. <a href="">Reload</a> to refresh your session.</span>
      <span class="signed-out-tab-flash">You signed out in another tab or window. <a href="">Reload</a> to refresh your session.</span>
    </div>
    <div class="facebox" id="facebox" style="display:none;">
  <div class="facebox-popup">
    <div class="facebox-content" role="dialog" aria-labelledby="facebox-header" aria-describedby="facebox-description">
    </div>
    <button type="button" class="facebox-close js-facebox-close" aria-label="Close modal">
      <svg aria-hidden="true" class="octicon octicon-x" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M7.48 8l3.75 3.75-1.48 1.48L6 9.48l-3.75 3.75-1.48-1.48L4.52 8 .77 4.25l1.48-1.48L6 6.52l3.75-3.75 1.48 1.48z"></path></svg>
    </button>
  </div>
</div>

  </body>
</html>

