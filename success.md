



<!DOCTYPE html>
<html lang="en" class="">
  <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# object: http://ogp.me/ns/object# article: http://ogp.me/ns/article# profile: http://ogp.me/ns/profile#">
    <meta charset='utf-8'>

    <link crossorigin="anonymous" href="https://assets-cdn.github.com/assets/frameworks-46094950e058d2cf30542982edfa17467b40fef171a28a786bdae50fab15526e.css" media="all" rel="stylesheet" />
    <link crossorigin="anonymous" href="https://assets-cdn.github.com/assets/github-65eae0e61ee036b2612ade2e43f38c8dea398a79f76be2aa2abebc4e52d4ccbe.css" media="all" rel="stylesheet" />
    
    
    <link crossorigin="anonymous" href="https://assets-cdn.github.com/assets/site-3b27741f2e22b48854ecee715112d86949a7c9e4e502f41739c4578cc65283da.css" media="all" rel="stylesheet" />
    
    

    <link as="script" href="https://assets-cdn.github.com/assets/frameworks-f2de4c863a487a877150989f965232bacfde178abf9c1963d9f84c5c19916f0c.js" rel="preload" />
    
    <link as="script" href="https://assets-cdn.github.com/assets/github-8922cd0a72e7d62c01c9622b2ac8c62afcd4114585de13349346cc789d65ad81.js" rel="preload" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta name="viewport" content="width=device-width">
    
    
    <title>ocaml.org/success.md at master · ocaml/ocaml.org · GitHub</title>
    <link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml" title="GitHub">
    <link rel="fluid-icon" href="https://github.com/fluidicon.png" title="GitHub">
    <link rel="apple-touch-icon" href="/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
    <meta property="fb:app_id" content="1401488693436528">

      <meta content="https://avatars3.githubusercontent.com/u/1841483?v=3&amp;s=400" name="twitter:image:src" /><meta content="@github" name="twitter:site" /><meta content="summary" name="twitter:card" /><meta content="ocaml/ocaml.org" name="twitter:title" /><meta content="Implementation of the ocaml.org website." name="twitter:description" />
      <meta content="https://avatars3.githubusercontent.com/u/1841483?v=3&amp;s=400" property="og:image" /><meta content="GitHub" property="og:site_name" /><meta content="object" property="og:type" /><meta content="ocaml/ocaml.org" property="og:title" /><meta content="https://github.com/ocaml/ocaml.org" property="og:url" /><meta content="Implementation of the ocaml.org website." property="og:description" />
      <meta name="browser-stats-url" content="https://api.github.com/_private/browser/stats">
    <meta name="browser-errors-url" content="https://api.github.com/_private/browser/errors">
    <link rel="assets" href="https://assets-cdn.github.com/">
    
    <meta name="pjax-timeout" content="1000">
    

    <meta name="msapplication-TileImage" content="/windows-tile.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="selected-link" value="repo_source" data-pjax-transient>

    <meta name="google-site-verification" content="KT5gs8h0wvaagLKAVWq8bbeNwnZZK1r1XQysX3xurLU">
<meta name="google-site-verification" content="ZzhVyEFwb7w3e0-uOTltm8Jsck2F5StVihD0exw2fsA">
    <meta name="google-analytics" content="UA-3769691-2">

<meta content="collector.githubapp.com" name="octolytics-host" /><meta content="github" name="octolytics-app-id" /><meta content="6ABA759E:65AB:CEB7916:577CBEEF" name="octolytics-dimension-request_id" />
<meta content="/&lt;user-name&gt;/&lt;repo-name&gt;/blob/show" data-pjax-transient="true" name="analytics-location" />



  <meta class="js-ga-set" name="dimension1" content="Logged Out">



        <meta name="hostname" content="github.com">
    <meta name="user-login" content="">

        <meta name="expected-hostname" content="github.com">
      <meta name="js-proxy-site-detection-payload" content="YTY2YzlmZDBlZDRjYzBmMDNhMjlmYjc2NGUzYTZhZTg3YjVkZDAwM2Y1NzFhZWM0NDA3NzY4YmRlNWY2ODQ2MXx7InJlbW90ZV9hZGRyZXNzIjoiMTA2LjE4Ni4xMTcuMTU4IiwicmVxdWVzdF9pZCI6IjZBQkE3NTlFOjY1QUI6Q0VCNzkxNjo1NzdDQkVFRiIsInRpbWVzdGFtcCI6MTQ2Nzc5MzEzNn0=">


      <link rel="mask-icon" href="https://assets-cdn.github.com/pinned-octocat.svg" color="#4078c0">
      <link rel="icon" type="image/x-icon" href="https://assets-cdn.github.com/favicon.ico">

    <meta name="html-safe-nonce" content="929fea5418371badb3477bf816f7586dfa6b4710">
    <meta content="a24686c8bdf1af0b56642c97a5b9a4d0b801ec6e" name="form-nonce" />

    <meta http-equiv="x-pjax-version" content="d43371b6616f0e920351e907f7dc41c3">
    

      
  <meta name="description" content="Implementation of the ocaml.org website.">
  <meta name="go-import" content="github.com/ocaml/ocaml.org git https://github.com/ocaml/ocaml.org.git">

  <meta content="1841483" name="octolytics-dimension-user_id" /><meta content="ocaml" name="octolytics-dimension-user_login" /><meta content="3417777" name="octolytics-dimension-repository_id" /><meta content="ocaml/ocaml.org" name="octolytics-dimension-repository_nwo" /><meta content="true" name="octolytics-dimension-repository_public" /><meta content="false" name="octolytics-dimension-repository_is_fork" /><meta content="3417777" name="octolytics-dimension-repository_network_root_id" /><meta content="ocaml/ocaml.org" name="octolytics-dimension-repository_network_root_nwo" />
  <link href="https://github.com/ocaml/ocaml.org/commits/master.atom" rel="alternate" title="Recent Commits to ocaml.org:master" type="application/atom+xml">


      <link rel="canonical" href="https://github.com/ocaml/ocaml.org/blob/master/site/learn/success.md" data-pjax-transient>
  </head>


  <body class="logged-out   env-production  vis-public page-blob">
    <div id="js-pjax-loader-bar" class="pjax-loader-bar"></div>
    <a href="#start-of-content" tabindex="1" class="accessibility-aid js-skip-to-content">Skip to content</a>

    
    
    



          <header class="site-header js-details-container" role="banner">
  <div class="container-responsive">
    <a class="header-logo-invertocat" href="https://github.com/" aria-label="Homepage" data-ga-click="(Logged out) Header, go to homepage, icon:logo-wordmark">
      <svg aria-hidden="true" class="octicon octicon-mark-github" height="32" version="1.1" viewBox="0 0 16 16" width="32"><path d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0 0 16 8c0-4.42-3.58-8-8-8z"></path></svg>
    </a>

    <button class="btn-link right site-header-toggle js-details-target" type="button" aria-label="Toggle navigation">
      <svg aria-hidden="true" class="octicon octicon-three-bars" height="24" version="1.1" viewBox="0 0 12 16" width="18"><path d="M11.41 9H.59C0 9 0 8.59 0 8c0-.59 0-1 .59-1H11.4c.59 0 .59.41.59 1 0 .59 0 1-.59 1h.01zm0-4H.59C0 5 0 4.59 0 4c0-.59 0-1 .59-1H11.4c.59 0 .59.41.59 1 0 .59 0 1-.59 1h.01zM.59 11H11.4c.59 0 .59.41.59 1 0 .59 0 1-.59 1H.59C0 13 0 12.59 0 12c0-.59 0-1 .59-1z"></path></svg>
    </button>

    <div class="site-header-menu">
      <nav class="site-header-nav site-header-nav-main">
        <a href="/personal" class="js-selected-navigation-item nav-item nav-item-personal" data-ga-click="Header, click, Nav menu - item:personal" data-selected-links="/personal /personal">
          Personal
</a>        <a href="/open-source" class="js-selected-navigation-item nav-item nav-item-opensource" data-ga-click="Header, click, Nav menu - item:opensource" data-selected-links="/open-source /open-source">
          Open source
</a>        <a href="/business" class="js-selected-navigation-item nav-item nav-item-business" data-ga-click="Header, click, Nav menu - item:business" data-selected-links="/business /business/features /business/customers /business">
          Business
</a>        <a href="/explore" class="js-selected-navigation-item nav-item nav-item-explore" data-ga-click="Header, click, Nav menu - item:explore" data-selected-links="/explore /trending /trending/developers /integrations /integrations/feature/code /integrations/feature/collaborate /integrations/feature/ship /explore">
          Explore
</a>      </nav>

      <div class="site-header-actions">
            <a class="btn btn-primary site-header-actions-btn" href="/join?source=header-repo" data-ga-click="(Logged out) Header, clicked Sign up, text:sign-up">Sign up</a>
          <a class="btn site-header-actions-btn mr-2" href="/login?return_to=%2Focaml%2Focaml.org%2Fblob%2Fmaster%2Fsite%2Flearn%2Fsuccess.md" data-ga-click="(Logged out) Header, clicked Sign in, text:sign-in">Sign in</a>
      </div>

        <nav class="site-header-nav site-header-nav-secondary">
          <a class="nav-item" href="/pricing">Pricing</a>
          <a class="nav-item" href="/blog">Blog</a>
          <a class="nav-item" href="https://help.github.com">Support</a>
          <a class="nav-item header-search-link" href="https://github.com/search">Search GitHub</a>
              <div class="header-search scoped-search site-scoped-search js-site-search" role="search">
  <!-- </textarea> --><!-- '"` --><form accept-charset="UTF-8" action="/ocaml/ocaml.org/search" class="js-site-search-form" data-scoped-search-url="/ocaml/ocaml.org/search" data-unscoped-search-url="/search" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
    <label class="form-control header-search-wrapper js-chromeless-input-container">
      <div class="header-search-scope">This repository</div>
      <input type="text"
        class="form-control header-search-input js-site-search-focus js-site-search-field is-clearable"
        data-hotkey="s"
        name="q"
        placeholder="Search"
        aria-label="Search this repository"
        data-unscoped-placeholder="Search GitHub"
        data-scoped-placeholder="Search"
        tabindex="1"
        autocapitalize="off">
    </label>
</form></div>

        </nav>
    </div>
  </div>
</header>



    <div id="start-of-content" class="accessibility-aid"></div>

      <div id="js-flash-container">
</div>


    <div role="main" class="main-content">
        <div itemscope itemtype="http://schema.org/SoftwareSourceCode">
    <div id="js-repo-pjax-container" data-pjax-container>
      
<div class="pagehead repohead instapaper_ignore readability-menu experiment-repo-nav">
  <div class="container repohead-details-container">

    

<ul class="pagehead-actions">

  <li>
      <a href="/login?return_to=%2Focaml%2Focaml.org"
    class="btn btn-sm btn-with-count tooltipped tooltipped-n"
    aria-label="You must be signed in to watch a repository" rel="nofollow">
    <svg aria-hidden="true" class="octicon octicon-eye" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M8.06 2C3 2 0 8 0 8s3 6 8.06 6C13 14 16 8 16 8s-3-6-7.94-6zM8 12c-2.2 0-4-1.78-4-4 0-2.2 1.8-4 4-4 2.22 0 4 1.8 4 4 0 2.22-1.78 4-4 4zm2-4c0 1.11-.89 2-2 2-1.11 0-2-.89-2-2 0-1.11.89-2 2-2 1.11 0 2 .89 2 2z"></path></svg>
    Watch
  </a>
  <a class="social-count" href="/ocaml/ocaml.org/watchers">
    29
  </a>

  </li>

  <li>
      <a href="/login?return_to=%2Focaml%2Focaml.org"
    class="btn btn-sm btn-with-count tooltipped tooltipped-n"
    aria-label="You must be signed in to star a repository" rel="nofollow">
    <svg aria-hidden="true" class="octicon octicon-star" height="16" version="1.1" viewBox="0 0 14 16" width="14"><path d="M14 6l-4.9-.64L7 1 4.9 5.36 0 6l3.6 3.26L2.67 14 7 11.67 11.33 14l-.93-4.74z"></path></svg>
    Star
  </a>

    <a class="social-count js-social-count" href="/ocaml/ocaml.org/stargazers">
      131
    </a>

  </li>

  <li>
      <a href="/login?return_to=%2Focaml%2Focaml.org"
        class="btn btn-sm btn-with-count tooltipped tooltipped-n"
        aria-label="You must be signed in to fork a repository" rel="nofollow">
        <svg aria-hidden="true" class="octicon octicon-repo-forked" height="16" version="1.1" viewBox="0 0 10 16" width="10"><path d="M8 1a1.993 1.993 0 0 0-1 3.72V6L5 8 3 6V4.72A1.993 1.993 0 0 0 2 1a1.993 1.993 0 0 0-1 3.72V6.5l3 3v1.78A1.993 1.993 0 0 0 5 15a1.993 1.993 0 0 0 1-3.72V9.5l3-3V4.72A1.993 1.993 0 0 0 8 1zM2 4.2C1.34 4.2.8 3.65.8 3c0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2zm3 10c-.66 0-1.2-.55-1.2-1.2 0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2zm3-10c-.66 0-1.2-.55-1.2-1.2 0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2z"></path></svg>
        Fork
      </a>

    <a href="/ocaml/ocaml.org/network" class="social-count">
      173
    </a>
  </li>
</ul>

    <h1 class="public ">
  <svg aria-hidden="true" class="octicon octicon-repo" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M4 9H3V8h1v1zm0-3H3v1h1V6zm0-2H3v1h1V4zm0-2H3v1h1V2zm8-1v12c0 .55-.45 1-1 1H6v2l-1.5-1.5L3 16v-2H1c-.55 0-1-.45-1-1V1c0-.55.45-1 1-1h10c.55 0 1 .45 1 1zm-1 10H1v2h2v-1h3v1h5v-2zm0-10H2v9h9V1z"></path></svg>
  <span class="author" itemprop="author"><a href="/ocaml" class="url fn" rel="author">ocaml</a></span><!--
--><span class="path-divider">/</span><!--
--><strong itemprop="name"><a href="/ocaml/ocaml.org" data-pjax="#js-repo-pjax-container">ocaml.org</a></strong>

</h1>

  </div>
  <div class="container">
    
<nav class="reponav js-repo-nav js-sidenav-container-pjax"
     itemscope
     itemtype="http://schema.org/BreadcrumbList"
     role="navigation"
     data-pjax="#js-repo-pjax-container">

  <span itemscope itemtype="http://schema.org/ListItem" itemprop="itemListElement">
    <a href="/ocaml/ocaml.org" aria-selected="true" class="js-selected-navigation-item selected reponav-item" data-hotkey="g c" data-selected-links="repo_source repo_downloads repo_commits repo_releases repo_tags repo_branches /ocaml/ocaml.org" itemprop="url">
      <svg aria-hidden="true" class="octicon octicon-code" height="16" version="1.1" viewBox="0 0 14 16" width="14"><path d="M9.5 3L8 4.5 11.5 8 8 11.5 9.5 13 14 8 9.5 3zm-5 0L0 8l4.5 5L6 11.5 2.5 8 6 4.5 4.5 3z"></path></svg>
      <span itemprop="name">Code</span>
      <meta itemprop="position" content="1">
</a>  </span>

    <span itemscope itemtype="http://schema.org/ListItem" itemprop="itemListElement">
      <a href="/ocaml/ocaml.org/issues" class="js-selected-navigation-item reponav-item" data-hotkey="g i" data-selected-links="repo_issues repo_labels repo_milestones /ocaml/ocaml.org/issues" itemprop="url">
        <svg aria-hidden="true" class="octicon octicon-issue-opened" height="16" version="1.1" viewBox="0 0 14 16" width="14"><path d="M7 2.3c3.14 0 5.7 2.56 5.7 5.7s-2.56 5.7-5.7 5.7A5.71 5.71 0 0 1 1.3 8c0-3.14 2.56-5.7 5.7-5.7zM7 1C3.14 1 0 4.14 0 8s3.14 7 7 7 7-3.14 7-7-3.14-7-7-7zm1 3H6v5h2V4zm0 6H6v2h2v-2z"></path></svg>
        <span itemprop="name">Issues</span>
        <span class="counter">114</span>
        <meta itemprop="position" content="2">
</a>    </span>

  <span itemscope itemtype="http://schema.org/ListItem" itemprop="itemListElement">
    <a href="/ocaml/ocaml.org/pulls" class="js-selected-navigation-item reponav-item" data-hotkey="g p" data-selected-links="repo_pulls /ocaml/ocaml.org/pulls" itemprop="url">
      <svg aria-hidden="true" class="octicon octicon-git-pull-request" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M11 11.28V5c-.03-.78-.34-1.47-.94-2.06C9.46 2.35 8.78 2.03 8 2H7V0L4 3l3 3V4h1c.27.02.48.11.69.31.21.2.3.42.31.69v6.28A1.993 1.993 0 0 0 10 15a1.993 1.993 0 0 0 1-3.72zm-1 2.92c-.66 0-1.2-.55-1.2-1.2 0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2zM4 3c0-1.11-.89-2-2-2a1.993 1.993 0 0 0-1 3.72v6.56A1.993 1.993 0 0 0 2 15a1.993 1.993 0 0 0 1-3.72V4.72c.59-.34 1-.98 1-1.72zm-.8 10c0 .66-.55 1.2-1.2 1.2-.65 0-1.2-.55-1.2-1.2 0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2zM2 4.2C1.34 4.2.8 3.65.8 3c0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2z"></path></svg>
      <span itemprop="name">Pull requests</span>
      <span class="counter">3</span>
      <meta itemprop="position" content="3">
</a>  </span>

    <a href="/ocaml/ocaml.org/wiki" class="js-selected-navigation-item reponav-item" data-hotkey="g w" data-selected-links="repo_wiki /ocaml/ocaml.org/wiki">
      <svg aria-hidden="true" class="octicon octicon-book" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M3 5h4v1H3V5zm0 3h4V7H3v1zm0 2h4V9H3v1zm11-5h-4v1h4V5zm0 2h-4v1h4V7zm0 2h-4v1h4V9zm2-6v9c0 .55-.45 1-1 1H9.5l-1 1-1-1H2c-.55 0-1-.45-1-1V3c0-.55.45-1 1-1h5.5l1 1 1-1H15c.55 0 1 .45 1 1zm-8 .5L7.5 3H2v9h6V3.5zm7-.5H9.5l-.5.5V12h6V3z"></path></svg>
      Wiki
</a>

  <a href="/ocaml/ocaml.org/pulse" class="js-selected-navigation-item reponav-item" data-selected-links="pulse /ocaml/ocaml.org/pulse">
    <svg aria-hidden="true" class="octicon octicon-pulse" height="16" version="1.1" viewBox="0 0 14 16" width="14"><path d="M11.5 8L8.8 5.4 6.6 8.5 5.5 1.6 2.38 8H0v2h3.6l.9-1.8.9 5.4L9 8.5l1.6 1.5H14V8z"></path></svg>
    Pulse
</a>
  <a href="/ocaml/ocaml.org/graphs" class="js-selected-navigation-item reponav-item" data-selected-links="repo_graphs repo_contributors /ocaml/ocaml.org/graphs">
    <svg aria-hidden="true" class="octicon octicon-graph" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M16 14v1H0V0h1v14h15zM5 13H3V8h2v5zm4 0H7V3h2v10zm4 0h-2V6h2v7z"></path></svg>
    Graphs
</a>

</nav>

  </div>
</div>

<div class="container new-discussion-timeline experiment-repo-nav">
  <div class="repository-content">

    

<a href="/ocaml/ocaml.org/blob/6e9323604d841d4ce010893b11d69830b9934f75/site/learn/success.md" class="hidden js-permalink-shortcut" data-hotkey="y">Permalink</a>

<!-- blob contrib key: blob_contributors:v21:8428538e979cee6a98aca14ab3cb18ca -->

<div class="file-navigation js-zeroclipboard-container">
  
<div class="select-menu branch-select-menu js-menu-container js-select-menu left">
  <button class="btn btn-sm select-menu-button js-menu-target css-truncate" data-hotkey="w"
    title="master"
    type="button" aria-label="Switch branches or tags" tabindex="0" aria-haspopup="true">
    <i>Branch:</i>
    <span class="js-select-button css-truncate-target">master</span>
  </button>

  <div class="select-menu-modal-holder js-menu-content js-navigation-container" data-pjax aria-hidden="true">

    <div class="select-menu-modal">
      <div class="select-menu-header">
        <svg aria-label="Close" class="octicon octicon-x js-menu-close" height="16" role="img" version="1.1" viewBox="0 0 12 16" width="12"><path d="M7.48 8l3.75 3.75-1.48 1.48L6 9.48l-3.75 3.75-1.48-1.48L4.52 8 .77 4.25l1.48-1.48L6 6.52l3.75-3.75 1.48 1.48z"></path></svg>
        <span class="select-menu-title">Switch branches/tags</span>
      </div>

      <div class="select-menu-filters">
        <div class="select-menu-text-filter">
          <input type="text" aria-label="Filter branches/tags" id="context-commitish-filter-field" class="form-control js-filterable-field js-navigation-enable" placeholder="Filter branches/tags">
        </div>
        <div class="select-menu-tabs">
          <ul>
            <li class="select-menu-tab">
              <a href="#" data-tab-filter="branches" data-filter-placeholder="Filter branches/tags" class="js-select-menu-tab" role="tab">Branches</a>
            </li>
            <li class="select-menu-tab">
              <a href="#" data-tab-filter="tags" data-filter-placeholder="Find a tag…" class="js-select-menu-tab" role="tab">Tags</a>
            </li>
          </ul>
        </div>
      </div>

      <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket" data-tab-filter="branches" role="menu">

        <div data-filterable-for="context-commitish-filter-field" data-filterable-type="substring">


            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/ocaml/ocaml.org/blob/cohttp/site/learn/success.md"
               data-name="cohttp"
               data-skip-pjax="true"
               rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5l-8 8-4-4 1.5-1.5L4 10l6.5-6.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target js-select-menu-filter-text" title="cohttp">
                cohttp
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open selected"
               href="/ocaml/ocaml.org/blob/master/site/learn/success.md"
               data-name="master"
               data-skip-pjax="true"
               rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5l-8 8-4-4 1.5-1.5L4 10l6.5-6.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target js-select-menu-filter-text" title="master">
                master
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/ocaml/ocaml.org/blob/oloop/site/learn/success.md"
               data-name="oloop"
               data-skip-pjax="true"
               rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5l-8 8-4-4 1.5-1.5L4 10l6.5-6.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target js-select-menu-filter-text" title="oloop">
                oloop
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/ocaml/ocaml.org/blob/omd/site/learn/success.md"
               data-name="omd"
               data-skip-pjax="true"
               rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5l-8 8-4-4 1.5-1.5L4 10l6.5-6.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target js-select-menu-filter-text" title="omd">
                omd
              </span>
            </a>
        </div>

          <div class="select-menu-no-results">Nothing to show</div>
      </div>

      <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket" data-tab-filter="tags">
        <div data-filterable-for="context-commitish-filter-field" data-filterable-type="substring">


            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/ocaml/ocaml.org/tree/1.0/site/learn/success.md"
              data-name="1.0"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5l-8 8-4-4 1.5-1.5L4 10l6.5-6.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="1.0">
                1.0
              </span>
            </a>
        </div>

        <div class="select-menu-no-results">Nothing to show</div>
      </div>

    </div>
  </div>
</div>

  <div class="btn-group right">
    <a href="/ocaml/ocaml.org/find/master"
          class="js-pjax-capture-input btn btn-sm"
          data-pjax
          data-hotkey="t">
      Find file
    </a>
    <button aria-label="Copy file path to clipboard" class="js-zeroclipboard btn btn-sm zeroclipboard-button tooltipped tooltipped-s" data-copied-hint="Copied!" type="button">Copy path</button>
  </div>
  <div class="breadcrumb js-zeroclipboard-target">
    <span class="repo-root js-repo-root"><span class="js-path-segment"><a href="/ocaml/ocaml.org"><span>ocaml.org</span></a></span></span><span class="separator">/</span><span class="js-path-segment"><a href="/ocaml/ocaml.org/tree/master/site"><span>site</span></a></span><span class="separator">/</span><span class="js-path-segment"><a href="/ocaml/ocaml.org/tree/master/site/learn"><span>learn</span></a></span><span class="separator">/</span><strong class="final-path">success.md</strong>
  </div>
</div>


  <div class="commit-tease">
      <span class="right">
        <a class="commit-tease-sha" href="/ocaml/ocaml.org/commit/cf7646439ce5ed10d95f4785a56e0f547b2a436c" data-pjax>
          cf76464
        </a>
        <relative-time datetime="2015-07-18T15:17:08Z">Jul 18, 2015</relative-time>
      </span>
      <div>
        <img alt="@Chris00" class="avatar" height="20" src="https://avatars2.githubusercontent.com/u/1255665?v=3&amp;s=40" width="20" />
        <a href="/Chris00" class="user-mention" rel="contributor">Chris00</a>
          <a href="/ocaml/ocaml.org/commit/cf7646439ce5ed10d95f4785a56e0f547b2a436c" class="message" data-pjax="true" title="Fix LexiFi screenshot links">Fix LexiFi screenshot links</a>
      </div>

    <div class="commit-tease-contributors">
      <button type="button" class="btn-link muted-link contributors-toggle" data-facebox="#blob_contributors_box">
        <strong>5</strong>
         contributors
      </button>
          <a class="avatar-link tooltipped tooltipped-s" aria-label="Chris00" href="/ocaml/ocaml.org/commits/master/site/learn/success.md?author=Chris00"><img alt="@Chris00" class="avatar" height="20" src="https://avatars2.githubusercontent.com/u/1255665?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="agarwal" href="/ocaml/ocaml.org/commits/master/site/learn/success.md?author=agarwal"><img alt="@agarwal" class="avatar" height="20" src="https://avatars2.githubusercontent.com/u/112173?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="johnwhitington" href="/ocaml/ocaml.org/commits/master/site/learn/success.md?author=johnwhitington"><img alt="@johnwhitington" class="avatar" height="20" src="https://avatars0.githubusercontent.com/u/1702581?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="Teemperor" href="/ocaml/ocaml.org/commits/master/site/learn/success.md?author=Teemperor"><img alt="@Teemperor" class="avatar" height="20" src="https://avatars2.githubusercontent.com/u/736001?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="yminsky" href="/ocaml/ocaml.org/commits/master/site/learn/success.md?author=yminsky"><img alt="@yminsky" class="avatar" height="20" src="https://avatars2.githubusercontent.com/u/715302?v=3&amp;s=40" width="20" /> </a>


    </div>

    <div id="blob_contributors_box" style="display:none">
      <h2 class="facebox-header" data-facebox-id="facebox-header">Users who have contributed to this file</h2>
      <ul class="facebox-user-list" data-facebox-id="facebox-description">
          <li class="facebox-user-list-item">
            <img alt="@Chris00" height="24" src="https://avatars0.githubusercontent.com/u/1255665?v=3&amp;s=48" width="24" />
            <a href="/Chris00">Chris00</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@agarwal" height="24" src="https://avatars0.githubusercontent.com/u/112173?v=3&amp;s=48" width="24" />
            <a href="/agarwal">agarwal</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@johnwhitington" height="24" src="https://avatars2.githubusercontent.com/u/1702581?v=3&amp;s=48" width="24" />
            <a href="/johnwhitington">johnwhitington</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@Teemperor" height="24" src="https://avatars0.githubusercontent.com/u/736001?v=3&amp;s=48" width="24" />
            <a href="/Teemperor">Teemperor</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@yminsky" height="24" src="https://avatars0.githubusercontent.com/u/715302?v=3&amp;s=48" width="24" />
            <a href="/yminsky">yminsky</a>
          </li>
      </ul>
    </div>
  </div>

<div class="file">
  <div class="file-header">
  <div class="file-actions">

    <div class="btn-group">
      <a href="/ocaml/ocaml.org/raw/master/site/learn/success.md" class="btn btn-sm " id="raw-url">Raw</a>
        <a href="/ocaml/ocaml.org/blame/master/site/learn/success.md" class="btn btn-sm js-update-url-with-hash">Blame</a>
      <a href="/ocaml/ocaml.org/commits/master/site/learn/success.md" class="btn btn-sm " rel="nofollow">History</a>
    </div>


        <button type="button" class="btn-octicon disabled tooltipped tooltipped-nw"
          aria-label="You must be signed in to make or propose changes">
          <svg aria-hidden="true" class="octicon octicon-pencil" height="16" version="1.1" viewBox="0 0 14 16" width="14"><path d="M0 12v3h3l8-8-3-3-8 8zm3 2H1v-2h1v1h1v1zm10.3-9.3L12 6 9 3l1.3-1.3a.996.996 0 0 1 1.41 0l1.59 1.59c.39.39.39 1.02 0 1.41z"></path></svg>
        </button>
        <button type="button" class="btn-octicon btn-octicon-danger disabled tooltipped tooltipped-nw"
          aria-label="You must be signed in to make or propose changes">
          <svg aria-hidden="true" class="octicon octicon-trashcan" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M11 2H9c0-.55-.45-1-1-1H5c-.55 0-1 .45-1 1H2c-.55 0-1 .45-1 1v1c0 .55.45 1 1 1v9c0 .55.45 1 1 1h7c.55 0 1-.45 1-1V5c.55 0 1-.45 1-1V3c0-.55-.45-1-1-1zm-1 12H3V5h1v8h1V5h1v8h1V5h1v8h1V5h1v9zm1-10H2V3h9v1z"></path></svg>
        </button>
  </div>

  <div class="file-info">
      228 lines (194 sloc)
      <span class="file-info-divider"></span>
    11.6 KB
  </div>
</div>

  
  <div id="readme" class="readme blob instapaper_body">
    <article class="markdown-body entry-content" itemprop="text">

<h1><a id="user-content-success-stories" class="anchor" href="#success-stories" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Success Stories</h1>

<p><em>Table of contents</em></p>

<h2><a id="user-content-jane-street" class="anchor" href="#jane-street" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Jane Street</h2>

<p><a href="http://janestreet.com/technology/"><img src="/ocaml/ocaml.org/raw/master/img/users/jane-street.jpg" alt="Jane Street" style="max-width:100%;"></a></p>

<p>Jane Street is a proprietary trading firm that uses OCaml as its primary
development platform.  Our operation runs at a large scale,
generating billions of dollars of transactions every day from our offices 
in Hong Kong, London and New York, with strategies that span many asset classes,
time-zones and regulatory regimes.</p>

<p>Almost all of of our software is written in OCaml, from statistical
research code to systems-administration tools to our real-time trading 
infrastructure.  OCaml’s type system acts as a rich and
well-integrated set of static analysis tools that help improve the
quality of our code, catching bugs at the earliest possible stage.
Billions of dollars of transactions flow through our systems every day,
so getting it right matters.  At the same time, OCaml is highly productive,
helping us quickly adapt to changing market conditions.</p>

<p>Jane Street has been contributing open-source libraries back to the wider
community for many years, including Core, our alternative standard 
library, Async, a cooperative concurrency library,
and several syntax extensions like binprot and sexplib.  All of these can
be found at <a href="http://janestreet.github.io">http://janestreet.github.io</a>.  All in, we've open-sourced 
more than 200k lines of code.</p>

<h2><a id="user-content-the-unison-file-synchronizer" class="anchor" href="#the-unison-file-synchronizer" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>The Unison File Synchronizer</h2>

<p><a href="/ocaml/ocaml.org/blob/master/img/unison.png"><img src="/ocaml/ocaml.org/raw/master/img/unison-thumb.jpg" alt="Screenshot" title="Screenshot of Unison's main window" style="max-width:100%;"></a></p>

<p><a href="http://www.cis.upenn.edu/%7Ebcpierce/unison/">Unison</a> is a popular
file-synchronization tool for Windows and most flavors of Unix. It
allows two replicas of a collection of files and directories to be
stored on different hosts (or different disks on the same host),
modified separately, and then brought up to date by propagating the
changes in each replica to the other. Unlike simple mirroring or backup
utilities, Unison can deal with updates to both replicas: updates that
do not conflict are propagated automatically and conflicting updates are
detected and displayed. Unison is also resilient to failure: it is
careful to leave the replicas and its own private structures in a
sensible state at all times, even in case of abnormal termination or
communication failures.</p>

<p><em><a href="http://www.cis.upenn.edu/%7Ebcpierce/">Benjamin C. Pierce</a> (University
of Pennsylvania), Unison project leader, says:</em> “I think Unison is a
very clear success for OCaml – in particular, for the extreme
portability and overall excellent engineering of the compiler and
runtime system. OCaml's strong static typing, in combination with its
powerful module system, helped us organize a large and intricate
codebase with a high degree of encapsulation. This has allowed us to
maintain a high level of robustness through years of work by many
different programmers. In fact, Unison may be unique among large OCaml
projects in having been <em>translated</em> from Java to OCaml midway through
its development. Moving to OCaml was like a breath of fresh air.”</p>

<h2><a id="user-content-the-mldonkey-peer-to-peer-client" class="anchor" href="#the-mldonkey-peer-to-peer-client" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>The MLdonkey peer-to-peer client</h2>

<p><a href="/ocaml/ocaml.org/blob/master/img/mldonkey.jpg"><img src="/ocaml/ocaml.org/raw/master/img/mldonkey-thumb.jpg" alt="Screenshot" title="Screenshot of one of MLdonkey's windows" style="max-width:100%;"></a></p>

<p><a href="http://mldonkey.sourceforge.net/Main_Page">MLdonkey</a> is a
multi-platform multi-networks peer-to-peer client. It was the first
open-source client to access the eDonkey network. Today, MLdonkey
supports several other large networks, among which Overnet, Bittorrent,
Gnutella, Gnutella2, Fasttrack, Soulseek, Direct-Connect, and Opennap.
Searches can be conducted over several networks concurrently; files are
downloaded from and uploaded to multiple peers concurrently.</p>

<p><em>An MLdonkey developer says:</em> “Early in 2002, we decided to use OCaml to
program a network application in the emerging world of peer-to-peer
systems. The result of our work, MLdonkey, has surpassed our hopes:
MLdonkey is currently the most popular peer-to-peer file-sharing client
according to <a href="http://freecode.com/">free(code)</a> (formerly “freshmeat.net”),
with about 10,000
daily users. Moreover, MLdonkey is the only client able to connect to
several peer-to-peer networks, to download and share files. It works as
a daemon, running unattended on the computer, and can be controlled
remotely using a choice of three different kinds of interfaces: GTK, web
and telnet.”</p>

<h2><a id="user-content-lexifis-modeling-language-for-finance" class="anchor" href="#lexifis-modeling-language-for-finance" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>LexiFi's Modeling Language for Finance</h2>

<p><a href="/ocaml/ocaml.org/blob/master/img/lexifi.jpg"><img src="/ocaml/ocaml.org/raw/master/img/lexifi-thumb.jpg" alt="Screenshot" title="A report produced by LexiFi software" style="max-width:100%;"></a></p>

<p>Developed by the company <a href="http://www.lexifi.com/">LexiFi</a>, the Modeling
Language for Finance (MLFi) is the first formal language that accurately
describes the most sophisticated capital market, credit, and investment
products. MLFi is implemented as an extension of OCaml.</p>

<p>MLFi users derive two important benefits from a functional programming
approach. First, the declarative formalism of functional programming
languages is well suited for <em>specifying</em> complex data structures and
algorithms. Second, functional programming languages have strong <em>list
processing</em> capabilities. Lists play a central role in finance where
they are used extensively to define contract event and payment
schedules.</p>

<p>In addition, MLFi provides crucial business integration capabilities
inherited from OCaml and related tools and libraries. This enables
users, for example, to interoperate with C and Java programs, manipulate
XML schemas and documents, and interface with SQL databases.</p>

<p>Data models and object models aiming to encapsulate the definitions and
behavior of financial instruments were developed by the banking industry
over the past two decades, but face inherent limitations that OCaml
helped overcome.</p>

<p>LexiFi's approach to modeling complex financial contracts received an
academic award in 2000, and the MLFi implementation was elected
“Software Product of the Year 2001” by the magazine <em>Risk</em>, the leading
financial trading and risk management publication. MLFi-based solutions
are gaining growing acceptance throughout Europe and are contributing to
spread the use of OCaml in the financial services industry.</p>

<h2><a id="user-content-the-coq-proof-assistant" class="anchor" href="#the-coq-proof-assistant" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>The Coq Proof Assistant</h2>

<p><a href="/ocaml/ocaml.org/blob/master/img/coq.jpg"><img src="/ocaml/ocaml.org/raw/master/img/coq-thumb.jpg" alt="Screenshot" title="Screenshot of Coq's integrated development environment" style="max-width:100%;"></a></p>

<p><em><a href="https://www.lri.fr/%7Efilliatr/">Jean-Christophe Filliâtre</a> (CNRS), a
Coq developer, says:</em> “The <a href="http://coq.inria.fr/">Coq</a> tool is a system
for manipulating formal mathematical proofs; a proof carried out in Coq
is mechanically verified by the machine. In addition to its applications
in mathematics, Coq also allows certifying the correctness of computer
programs.”</p>

<p>“From the Coq standpoint, OCaml is attractive on multiple grounds.
First, the OCaml language is perfectly suited for symbolic
manipulations, which are of paramount importance in a proof assistant.
Furthermore, OCaml's type system, and particularly its notion of
abstract type, allow securely encapsulating Coq's critical code base,
which guarantees the logical consistency of the whole system. Last,
OCaml's strong type system <em>de facto</em> grants Coq's code a high level of
quality: errors such as “segmentation faults” cannot occur during
execution, which is indispensable for a tool whose primary goal is
precisely rigor.”</p>

<h2><a id="user-content-the-astrÉe-static-analyzer" class="anchor" href="#the-astrÉe-static-analyzer" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>The ASTRÉE Static Analyzer</h2>

<p><a href="http://www.airbus.com/"><img src="/ocaml/ocaml.org/raw/master/img/astree.gif" alt="A340" title="ASTRÉE has been used to certify the Airbus A340 flight control software" style="max-width:100%;"></a></p>

<p><em><a href="http://www-verimag.imag.fr/%7Emonniaux/">David Monniaux</a> (CNRS), member
of the ASTRÉE project, says:</em> “<a href="http://www.astree.ens.fr/">ASTRÉE</a> is a
<em>static analyzer</em> based on <a href="http://www.di.ens.fr/%7Ecousot/aiintro.shtml"><em>abstract
interpretation</em></a> that aims
at proving the absence of runtime errors in safety-critical software
written in a subset of the C programming language.”</p>

<p>“Automatically analyzing programs for exactly checking properties such
as the absence of runtime errors is impossible in general, for
mathematical reasons. Static analysis by abstract interpretation works
around this impossibility and proves program properties by
over-approximating the possible behaviors of the program: it is possible
to design pessimistic approximations that, in practice, allow proving
the desired property on a wide range of software.”</p>

<p>“So far, ASTRÉE has proved the absence of runtime errors in the primary
control software of the <a href="http://www.airbus.com/product/a330_a340_backgrounder.asp">Airbus A340
family</a>. This
would be impossible by <em>software testing</em>, for testing only considers a
limited <em>subset</em> of the test cases, while abstract interpretation
considers a <em>superset</em> of all possible outcomes of the system.”</p>

<p>“<a href="http://www.astree.ens.fr/">ASTRÉE</a> is written in OCaml and is about
44000 lines long (plus external libraries). We needed a language with
good performance (speed and memory usage) on reasonable equipment, easy
support for advanced data structures, and type and memory safety. OCaml
also allows for modular, clear and compact source code and makes it easy
to work with recursive structures such as syntax trees.”</p>

<h2><a id="user-content-slam" class="anchor" href="#slam" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>SLAM</h2>

<p>The <a href="http://research.microsoft.com/en-us/projects/slam/">SLAM</a> project
originated in Microsoft Research in early 2000. Its goal was to
automatically check that a C program correctly uses the interface to an
external library. The project used and extended ideas from symbolic
model checking, program analysis and theorem proving in novel ways to
address this problem. The SLAM analysis engine forms the core of a new
tool called Static Driver Verifier (SDV) that systematically analyzes
the source code of Windows device drivers against a set of rules that
define what it means for a device driver to properly interact with the
Windows operating system kernel.</p>

<p><em>In technical report
<a href="http://research.microsoft.com/apps/pubs/default.aspx?id=70038">MSR-TR-2004-08</a>,
T.Ball, B.Cook, V.Levin and S.K.Rajamani, the SLAM developers, write:</em>
“The Right Tools for the Job: We developed SLAM using Inria's OCaml
functional programming language. The expressiveness of this language and
robustness of its implementation provided a great productivity boost.”</p>

<h2><a id="user-content-fftw" class="anchor" href="#fftw" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>FFTW</h2>

<p><a href="/ocaml/ocaml.org/blob/master/img/fftw-thumb.png" target="_blank"><img src="/ocaml/ocaml.org/raw/master/img/fftw-thumb.png" alt="FFTW" style="max-width:100%;"></a></p>

<p><a href="http://www.fftw.org/">FFTW</a> is a <a href="http://www.fftw.org/benchfft/">very
fast</a> C library for computing Discrete
Fourier Transforms (DFT). It uses a powerful symbolic optimizer written
in OCaml which, given an integer N, generates highly optimized C code to
compute DFTs of size N. FFTW was awarded the 1999 <a href="http://www.mcs.anl.gov/about-us/j-h-wilkinson-prize-numerical-software">Wilkinson
prize</a>
for numerical software.</p>

<p>Benchmarks, performed on on a variety of platforms, show that FFTW's
performance is typically superior to that of other publicly available
DFT software, and is even competitive with vendor-tuned codes. In
contrast to vendor-tuned codes, however, FFTW's performance is portable:
the same program will perform well on most architectures without
modification. Hence the name, “FFTW,” which stands for the somewhat
whimsical title of “Fastest Fourier Transform in the West.”</p>

<h2><a id="user-content-liquidsoap" class="anchor" href="#liquidsoap" aria-hidden="true"><svg aria-hidden="true" class="octicon octicon-link" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Liquidsoap</h2>

<p><a href="http://liquidsoap.fm/">Liquidsoap</a> is clearly well established in the
(internet) radio industry. Liquidsoap is well known as a tool with
unique abilities, and has lots of users including big commercial ones.
It is not developed as a business, but companies develop services or
software on top of it. For example, Sourcefabric develops and sells
Airtime on top of Liquidsoap.</p>
</article>
  </div>

</div>

<button type="button" data-facebox="#jump-to-line" data-facebox-class="linejump" data-hotkey="l" class="hidden">Jump to Line</button>
<div id="jump-to-line" style="display:none">
  <!-- </textarea> --><!-- '"` --><form accept-charset="UTF-8" action="" class="js-jump-to-line-form" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
    <input class="form-control linejump-input js-jump-to-line-field" type="text" placeholder="Jump to line&hellip;" aria-label="Jump to line" autofocus>
    <button type="submit" class="btn">Go</button>
</form></div>

  </div>
  <div class="modal-backdrop js-touch-events"></div>
</div>


    </div>
  </div>

    </div>

        <div class="container site-footer-container">
  <div class="site-footer" role="contentinfo">
    <ul class="site-footer-links right">
        <li><a href="https://status.github.com/" data-ga-click="Footer, go to status, text:status">Status</a></li>
      <li><a href="https://developer.github.com" data-ga-click="Footer, go to api, text:api">API</a></li>
      <li><a href="https://training.github.com" data-ga-click="Footer, go to training, text:training">Training</a></li>
      <li><a href="https://shop.github.com" data-ga-click="Footer, go to shop, text:shop">Shop</a></li>
        <li><a href="https://github.com/blog" data-ga-click="Footer, go to blog, text:blog">Blog</a></li>
        <li><a href="https://github.com/about" data-ga-click="Footer, go to about, text:about">About</a></li>

    </ul>

    <a href="https://github.com" aria-label="Homepage" class="site-footer-mark" title="GitHub">
      <svg aria-hidden="true" class="octicon octicon-mark-github" height="24" version="1.1" viewBox="0 0 16 16" width="24"><path d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0 0 16 8c0-4.42-3.58-8-8-8z"></path></svg>
</a>
    <ul class="site-footer-links">
      <li>&copy; 2016 <span title="0.04807s from github-fe164-cp1-prd.iad.github.net">GitHub</span>, Inc.</li>
        <li><a href="https://github.com/site/terms" data-ga-click="Footer, go to terms, text:terms">Terms</a></li>
        <li><a href="https://github.com/site/privacy" data-ga-click="Footer, go to privacy, text:privacy">Privacy</a></li>
        <li><a href="https://github.com/security" data-ga-click="Footer, go to security, text:security">Security</a></li>
        <li><a href="https://github.com/contact" data-ga-click="Footer, go to contact, text:contact">Contact</a></li>
        <li><a href="https://help.github.com" data-ga-click="Footer, go to help, text:help">Help</a></li>
    </ul>
  </div>
</div>



    

    <div id="ajax-error-message" class="ajax-error-message flash flash-error">
      <svg aria-hidden="true" class="octicon octicon-alert" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M8.865 1.52c-.18-.31-.51-.5-.87-.5s-.69.19-.87.5L.275 13.5c-.18.31-.18.69 0 1 .19.31.52.5.87.5h13.7c.36 0 .69-.19.86-.5.17-.31.18-.69.01-1L8.865 1.52zM8.995 13h-2v-2h2v2zm0-3h-2V6h2v4z"></path></svg>
      <button type="button" class="flash-close js-flash-close js-ajax-error-dismiss" aria-label="Dismiss error">
        <svg aria-hidden="true" class="octicon octicon-x" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M7.48 8l3.75 3.75-1.48 1.48L6 9.48l-3.75 3.75-1.48-1.48L4.52 8 .77 4.25l1.48-1.48L6 6.52l3.75-3.75 1.48 1.48z"></path></svg>
      </button>
      Something went wrong with that request. Please try again.
    </div>


      <script crossorigin="anonymous" src="https://assets-cdn.github.com/assets/compat-7db58f8b7b91111107fac755dd8b178fe7db0f209ced51fc339c446ad3f8da2b.js"></script>
      <script crossorigin="anonymous" src="https://assets-cdn.github.com/assets/frameworks-f2de4c863a487a877150989f965232bacfde178abf9c1963d9f84c5c19916f0c.js"></script>
      <script async="async" crossorigin="anonymous" src="https://assets-cdn.github.com/assets/github-8922cd0a72e7d62c01c9622b2ac8c62afcd4114585de13349346cc789d65ad81.js"></script>
      
      
      
      
      
      
    <div class="js-stale-session-flash stale-session-flash flash flash-warn flash-banner hidden">
      <svg aria-hidden="true" class="octicon octicon-alert" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M8.865 1.52c-.18-.31-.51-.5-.87-.5s-.69.19-.87.5L.275 13.5c-.18.31-.18.69 0 1 .19.31.52.5.87.5h13.7c.36 0 .69-.19.86-.5.17-.31.18-.69.01-1L8.865 1.52zM8.995 13h-2v-2h2v2zm0-3h-2V6h2v4z"></path></svg>
      <span class="signed-in-tab-flash">You signed in with another tab or window. <a href="">Reload</a> to refresh your session.</span>
      <span class="signed-out-tab-flash">You signed out in another tab or window. <a href="">Reload</a> to refresh your session.</span>
    </div>
    <div class="facebox" id="facebox" style="display:none;">
  <div class="facebox-popup">
    <div class="facebox-content" role="dialog" aria-labelledby="facebox-header" aria-describedby="facebox-description">
    </div>
    <button type="button" class="facebox-close js-facebox-close" aria-label="Close modal">
      <svg aria-hidden="true" class="octicon octicon-x" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M7.48 8l3.75 3.75-1.48 1.48L6 9.48l-3.75 3.75-1.48-1.48L4.52 8 .77 4.25l1.48-1.48L6 6.52l3.75-3.75 1.48 1.48z"></path></svg>
    </button>
  </div>
</div>

  </body>
</html>

